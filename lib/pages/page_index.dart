import 'package:flutter/material.dart';
import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:shop_accessory_app/components/components_accessory_item.dart';
import 'package:shop_accessory_app/model/accessory_item.dart';
import 'package:shop_accessory_app/pages/page_goods_detail.dart';
import 'package:shop_accessory_app/repository/repo_accessory.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {

  List<AccessoryItem> _list = [];

  Future<void> _loadList() async {
    await RepoAccessory().getAccessorys()
        .then((res) => {
          setState(() {
            _list = res.list;
          })
        })
        .catchError((err) => {
          debugPrint(err)
        });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  String searchValue = '';
  final List<String> _suggestions = ['Afeganistan', 'Albania', 'Algeria', 'Australia', 'Brazil', 'German', 'Madagascar', 'Mozambique', 'Portugal', 'Zambia'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EasySearchBar(
          title: const Text('Example'),
          backgroundColor: Colors.black45,
          titleTextStyle: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white60
          ),
          actions: [
            IconButton(onPressed: () {}, icon: Icon(Icons.add_alert_outlined)),
            IconButton(onPressed: () {}, icon: Icon(Icons.share)),
          ],
          iconTheme: IconThemeData(color: Colors.white),
          onSearch: (value) => setState(() => searchValue = value),
          suggestions: _suggestions
      ),
      drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.black45,
                ),
                child: Text('cold, smooth & tasty.'),
              ),
              ListTile(
                  title: const Text('Ring'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: const Text('Necklace'),
                  onTap: () => Navigator.pop(context)
              )
            ]
        ),
      ),
      body: _buildbody(context),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.black,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'),
          BottomNavigationBarItem(icon: Icon(Icons.add_chart), label: 'list'),
          BottomNavigationBarItem(icon: Icon(Icons.heart_broken), label: 'liked'),
          BottomNavigationBarItem(icon: Icon(Icons.tag_faces_rounded), label: 'mypage')
        ],
      ),
    );
  }

  Widget _buildbody(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Container(
          child: Column(
            children: [
              Text('cold, smooth & tasty.',
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.w700),
              ),
              Column(
                children: [
                  Container(
                    child: (
                    Image.asset('assets/beauty.gif')
                    ),
                  ),
                  Container(
                    child: Image.asset('assets/bigsale.png'),
                  )
                ],
              ),
              GridView.builder(
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                  itemCount: _list.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1 / 1,
                    mainAxisSpacing: 5,
                    crossAxisSpacing: 5,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                  return ComponentsAccessoryItem(
                      accessoryItem: _list[index],
                      callback: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(accessoryItem: _list[index], id: index)));
                      }
                  );
                  },
              ),
          ]),
        ),
      ),
    );
  }
}

