import 'package:flutter/material.dart';
import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:shop_accessory_app/model/accessory_item.dart';
import 'package:shop_accessory_app/repository/repo_accessory.dart';

class PageGoodsDetail extends StatefulWidget {
  const PageGoodsDetail({
    super.key,
    required this.accessoryItem,
    required this.id,
  });

  final AccessoryItem accessoryItem;
  final num id;

  @override
  State<PageGoodsDetail> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageGoodsDetail> {

  AccessoryItem? _detail;

  Future<void> _loadDetail() async {
    await RepoAccessory().getAccessory(widget.id)
        .then((res) => {
          setState(() {
            _detail =res.data;
          })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  String searchValue = '';
  final List<String> _suggestions = ['Afeganistan', 'Albania', 'Algeria', 'Australia', 'Brazil', 'German', 'Madagascar', 'Mozambique', 'Portugal', 'Zambia'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EasySearchBar(
          title: const Text('Example'),
          backgroundColor: Colors.black45,
          titleTextStyle: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.white60
          ),
          actions: [
            IconButton(onPressed: () {}, icon: Icon(Icons.add_alert_outlined)),
            IconButton(onPressed: () {}, icon: Icon(Icons.share)),
          ],
          iconTheme: IconThemeData(color: Colors.white),
          onSearch: (value) => setState(() => searchValue = value),
          suggestions: _suggestions
      ),
      drawer: Drawer(
        child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.black45,
                ),
                child: Text('Drawer Header'),
              ),
              ListTile(
                  title: const Text('Item 1'),
                  onTap: () => Navigator.pop(context)
              ),
              ListTile(
                  title: const Text('Item 2'),
                  onTap: () => Navigator.pop(context)
              )
            ]
        ),
      ),

      body: _buildbody(context),
    );
  }
  Widget _buildbody(BuildContext context) {
    if (_detail == null) {
      return Text('데이터 로딩중..');
    } else {
      return SingleChildScrollView(
          child: Center(
            child: Container(
              child: Column(
                children: [
                  Text('cold, smooth & tasty.',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 40, fontWeight: FontWeight.w700),
                  ),
                  Text('angelo brewing',),
                  Image.asset('assets/sale.png'),
                  Column(
                    children: [
                      Text('${widget.id}'),
                      Image.asset(widget.accessoryItem.imageName),
                      Text(widget.accessoryItem.name),
                      Text('${_detail!.price}')
                    ],
                  )
                ],
              ),
            ),
          )
      );
    }
  }
}