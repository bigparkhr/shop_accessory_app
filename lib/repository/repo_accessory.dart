import 'package:dio/dio.dart';
import 'package:shop_accessory_app/config/config_api.dart';
import 'package:shop_accessory_app/model/accessory_detail_result.dart';
import 'package:shop_accessory_app/model/shop_list_result.dart';

class RepoAccessory {
  Future<ShopListResult> getAccessorys() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/accessory/all';

    final response = await dio.get(
        _baseUrl,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            print(status);
            return status == 200;
          }
      )
    );
    return ShopListResult.formJson(response.data);
  }

  Future<AccessoryDetailResult> getAccessory(num id) async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/accessory/detail/{id}';
    
    final response = await dio.get(
      _baseUrl.replaceAll('{id}', id.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return AccessoryDetailResult.fromJson(response.data);
  }
}