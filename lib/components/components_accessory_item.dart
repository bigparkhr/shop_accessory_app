import 'package:flutter/material.dart';
import 'package:shop_accessory_app/model/accessory_item.dart';

class ComponentsAccessoryItem extends StatelessWidget {
  const ComponentsAccessoryItem({
    super.key,
    required this.accessoryItem,
    required this.callback
  });

  final AccessoryItem accessoryItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container( height: 100,
        child: Column(
          children: [
            Image.asset('${accessoryItem.imageName}', width: 170, height: 170,),
            Container(
              child:
              Row( mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text('${accessoryItem.name}', style: TextStyle (fontSize: 20)),
                  ),
                  Center(
                    child: Text('${accessoryItem.price}', style: TextStyle (fontSize: 20)),
                  )
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}
