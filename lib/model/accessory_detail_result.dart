import 'package:shop_accessory_app/model/accessory_item.dart';

class AccessoryDetailResult {
  String msg;
  num id;
  AccessoryItem data;

  AccessoryDetailResult(this.msg, this.id, this.data);

  factory AccessoryDetailResult.fromJson(Map<String, dynamic> json) {
    return AccessoryDetailResult(
        json['msg'],
        json['id'],
        AccessoryItem.fromJSon(json['data']),
    );
  }
}