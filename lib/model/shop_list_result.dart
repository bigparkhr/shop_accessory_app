import 'package:shop_accessory_app/model/accessory_item.dart';

class ShopListResult {
  String msg;
  num code;
  List<AccessoryItem> list;
  num totalCount;

  ShopListResult(this.msg, this.code, this.list, this.totalCount);

  factory ShopListResult.formJson(Map<String, dynamic> json) {
    return ShopListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
          (json['list'] as List).map((e) => AccessoryItem.fromJSon(e)).toList() : [],
      json['totalCount'],
    );
  }
}