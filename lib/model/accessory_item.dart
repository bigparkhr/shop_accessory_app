class AccessoryItem {
  num id;
  String imageName;
  String name;
  num price;

  AccessoryItem(this.id, this.imageName, this.name, this.price);


  factory AccessoryItem.fromJSon(Map<String, dynamic> json) {
    return AccessoryItem(
      json['id'],
      json['imageName'],
      json['name'],
      json['price'],
    );
  }
}